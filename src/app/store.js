import { combineReducers } from "redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import userListReducer from "../reducer/userList.reducer";


//root chứa các task reducer 
const rootReducer = combineReducers({
	//gọi các task
	userListReducer,
});

//tạo store
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;