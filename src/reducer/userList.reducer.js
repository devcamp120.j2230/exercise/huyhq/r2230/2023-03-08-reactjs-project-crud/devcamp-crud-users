import { MODAL_ADD_SET, MODAL_DELETE_SET, MODAL_UPDATE_SET, USERS_ADD_NEW, USERS_DELETE_ID, USERS_EDIT_ID, USERS_FETCH_ERROR, USERS_FETCH_PENDING, USERS_FETCH_SUCCESS } from "../constants/user.constant";

const taskList = {
    users: [],
    pending: false,
    changeStatus: false,
    modalAddOpen: false,
    modalUpdateOpen: false,
    modalDeleteOpen: false,
};

const userListReducer = (state = taskList, action) => {
    switch (action.type) {
        case USERS_FETCH_PENDING:
            state.pending = true;
            break;
        case USERS_FETCH_SUCCESS:
            state.changeStatus = false;
            state.pending = false;
            state.users = action.data;
            break;
        case USERS_FETCH_ERROR:
            break;
        case USERS_ADD_NEW:
            state.changeStatus = true;
            break;
        case USERS_EDIT_ID:
            state.changeStatus = true;
            break;
        case USERS_DELETE_ID:
            state.changeStatus = true;
            break;
        case MODAL_ADD_SET:
            state.modalAddOpen = action.modalAddOpen;
            break;
        case MODAL_UPDATE_SET:
            state.modalUpdateOpen = action.modalUpdateOpen;
            break;
        case MODAL_DELETE_SET:
            state.modalDeleteOpen = action.modalDeleteOpen;
            break;
        default:
            break;
    }
    return { ...state };
};

export default userListReducer