import axios from "axios";
import { USERS_ADD_NEW, USERS_DELETE_ID, USERS_EDIT_ID, USERS_FETCH_BY_ID_SUCCESS, USERS_FETCH_ERROR, USERS_FETCH_PENDING, USERS_FETCH_SUCCESS } from "../constants/user.constant";

const url = "http://203.171.20.210:8080/crud-api/users/";

const axiosCall = async (url, body) => {
    const response = await axios(url, body);
    return response.data;
};

export const fetchUsers = () => {
    return async (dispatch) => {
        await dispatch({
            type: USERS_FETCH_PENDING
        });

        axiosCall(url)
            .then(result => {
                return dispatch({
                    type: USERS_FETCH_SUCCESS,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: USERS_FETCH_ERROR,
                    error: error
                })
            })
    }
};

export const addUserAction = (user) => {
    return async (dispatch) => {
        var config = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: user
        };

        await dispatch({
            type: USERS_FETCH_PENDING
        });

        axiosCall(url, config)
            .then(result => {
                console.log(result);
                return dispatch({
                    type: USERS_ADD_NEW,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: USERS_FETCH_ERROR,
                    error: error
                })
            })

    }
};

export const updateUserAction = (user, userId) => {
    return async (dispatch) => {
        var config = {
            method: 'put',
            headers: {
                'Content-Type': 'application/json'
            },
            data: user
        };

        await dispatch({
            type: USERS_FETCH_PENDING
        });

        axiosCall(url+userId, config)
            .then(result => {
                console.log(result);
                return dispatch({
                    type: USERS_EDIT_ID,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: USERS_FETCH_ERROR,
                    error: error
                })
            })

    }
}

export const deleteUserAction = (userId) => {
    return async (dispatch) => {
        var config = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            },
        };

        await dispatch({
            type: USERS_FETCH_PENDING
        });

        axiosCall(url+userId, config)
            .then(result => {
                console.log(result);
                return dispatch({
                    type: USERS_DELETE_ID,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: USERS_FETCH_ERROR,
                    error: error
                })
            })

    }
}


