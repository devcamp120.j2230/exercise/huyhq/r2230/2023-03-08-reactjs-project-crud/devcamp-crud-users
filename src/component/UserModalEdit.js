import { Button, Modal, Box, Grid, TextField, InputLabel, MenuItem, FormControl, Select } from "@mui/material"
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateUserAction } from "../action/user.action";
import { MODAL_UPDATE_SET } from "../constants/user.constant";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const UserModalEdit = (props) => {
    const userId = props.userId;
    const { modalUpdateOpen } = useSelector((reduxData) => {
        return reduxData.userListReducer
    })
    
    const [userById, setUserById] = useState("")
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [subject, setSubject] = useState('');
    const [country, setCountry] = useState('');
    const [customerType, setCustomerType] = useState('');
    const [registerStatus, setRegisterStatus] = useState('');

    const dispatch = useDispatch();

    const url = "http://203.171.20.210:8080/crud-api/users/";

    const axiosCall = async (url, body) => {
        const response = await axios(url, body);
        return response.data;
    };

    useEffect(()=>{
        axiosCall(url+userId)
            .then(result => {
                console.log(result);
                setUserById(result);
            })
            .catch(error => {
                console.log(error);
            })
    },[userId]);

    const handleUpdateUser = () => {
        handleModalClose();
        dispatch(updateUserAction({
            firstname,
            lastname,
            subject,
            country,
            customerType,
            registerStatus
        },userId))
    }

    const handleModalOpen = () => {
        dispatch({
            type: MODAL_UPDATE_SET,
            modalUpdateOpen: true
        })
    };

    const handleModalClose = () => {
        dispatch({
            type: MODAL_UPDATE_SET,
            modalUpdateOpen: false
        })
    };

    const handleOnchangeFirstname = (e) => {
        setFirstname(e.target.value)
    }
    const handleOnchangeLastname = (e) => {
        setLastname(e.target.value)
    }
    const handleOnchangeSubject = (e) => {
        setSubject(e.target.value)
    }
    const handleChangeCountry = (e) => {
        setCountry(e.target.value);
    };
    const handleChangeType = (e) => {
        setCustomerType(e.target.value);
    };

    const handleChangeStatus = (e) => {
        setRegisterStatus(e.target.value);
    };

    return (
        <Modal
            open={modalUpdateOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container>
                    <Grid item xs={12} my={3}>
                        <TextField id="inp-first-name" label={userById.firstname} variant="outlined" onChange={handleOnchangeFirstname} fullWidth />
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <TextField id="inp-last-name" label={userById.lastname} variant="outlined" onChange={handleOnchangeLastname} fullWidth />
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <TextField id="inp-subject" label={userById.subject} variant="outlined" onChange={handleOnchangeSubject} fullWidth />
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <FormControl fullWidth>
                            <InputLabel id="label-country">{userById.country}</InputLabel>
                            <Select
                                id="select-country"
                                label={userById.country}
                                onChange={handleChangeCountry}
                            >
                                <MenuItem value="vietnam">Vietnam</MenuItem>
                                <MenuItem value="usa">USA</MenuItem>
                                <MenuItem value="china">China</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <FormControl fullWidth>
                            <InputLabel id="label-customer-type">{userById.customerType}</InputLabel>
                            <Select
                                id="select-type"
                                label={userById.customerType}
                                onChange={handleChangeType}
                            >
                                <MenuItem value="Standard">Standard</MenuItem>
                                <MenuItem value="Gold">Gold</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <FormControl fullWidth>
                            <InputLabel id="label-register-status">{userById.registerStatus}</InputLabel>
                            <Select
                                id="select-status"
                                label={userById.registerStatus}
                                onChange={handleChangeStatus}
                            >
                                <MenuItem value="New">New</MenuItem>
                                <MenuItem value="Open">Open</MenuItem>
                                <MenuItem value="Accepted">Accepted</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <Button sx={{ marginRight: 5 }} variant="contained" color="success" onClick={handleUpdateUser}>Sua</Button>
                        <Button onClick={handleModalClose} variant="contained" color="error">Huy</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
};

export default UserModalEdit;