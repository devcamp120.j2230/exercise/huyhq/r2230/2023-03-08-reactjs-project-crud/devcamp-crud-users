import { Button, Container, Typography } from "@mui/material"
import { useDispatch, useSelector } from "react-redux";
import { MODAL_ADD_SET } from "../constants/user.constant";
import UserListComponent from "./UserListComponent"
import UserModalAdd from "./UserModalAdd";

const UserComponent = () => {
    const dispatch = useDispatch();

    return (
        <Container>
            <Typography variant="h3" component="div" textAlign="Center">Danh sach User</Typography>
            <Button
                variant="contained"
                color="success"
                sx={{ margin: "10px" }}
                onClick={() => dispatch({
                    type: MODAL_ADD_SET,
                    modalAddOpen: true
                })}
            >
                Them User
            </Button>
            <UserListComponent />
            <UserModalAdd/>
        </Container>
    )
}

export default UserComponent;