import { Button, Modal, Box, Grid, TextField, InputLabel, MenuItem, FormControl, Select, Typography } from "@mui/material"
import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteUserAction, updateUserAction } from "../action/user.action";
import { MODAL_DELETE_SET, MODAL_UPDATE_SET } from "../constants/user.constant";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const UserModalDelete = (props) => {
    const userId = props.userId;
    const { modalDeleteOpen } = useSelector((reduxData) => {
        return reduxData.userListReducer
    })
    
    const dispatch = useDispatch();

    const handleDeleteUser = () => {
        handleModalClose();
        dispatch(deleteUserAction(userId))
    }

    const handleModalOpen = () => {
        dispatch({
            type: MODAL_DELETE_SET,
            modalDeleteOpen: true
        })
    };

    const handleModalClose = () => {
        dispatch({
            type: MODAL_DELETE_SET,
            modalDeleteOpen: false
        })
    };

    return (
        <Modal
            open={modalDeleteOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container>
                    <Grid item xs={12} my={3}>
                        <Typography variant="h4" component="div" textAlign="left">Confirm Delete User</Typography>
                    </Grid>
                    <Grid item xs={12} my={3}>
                    <Typography variant="p" component="div" textAlign="left">Bạn có muốn xóa User (id: {userId}) này!</Typography>
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <Button sx={{ marginRight: 5 }} variant="contained" color="error" onClick={handleDeleteUser}>Xoa</Button>
                        <Button onClick={handleModalClose} variant="contained" sx={{background:"grey"}}>Huy</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
};

export default UserModalDelete;