import { Button, Modal, Box, Grid, TextField, InputLabel, MenuItem, FormControl, Select } from "@mui/material"
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addUserAction } from "../action/user.action";
import { MODAL_ADD_SET } from "../constants/user.constant";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const UserModalAdd = () => {

    const {modalAddOpen} = useSelector((reduxData)=>{
        return reduxData.userListReducer
    })

    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [subject, setSubject] = useState('');
    const [country, setCountry] = useState('');
    const dispatch = useDispatch();

    const handleAddNewUser = () => {
        handleModalClose();
        dispatch(addUserAction({
            firstname,
            lastname,
            subject,
            country,
            customerType: "Standard"
        }))
    }

    const handleModalOpen = () => {
        dispatch({
            type: MODAL_ADD_SET,
            modalAddOpen: true
        })
    };

    const handleModalClose = () => {
        dispatch({
            type: MODAL_ADD_SET,
            modalAddOpen: false
        })
    };

    const handleOnchangeFirstname = (e) => {
        setFirstname(e.target.value)
    }
    const handleOnchangeLastname = (e) => {
        setLastname(e.target.value)
    }
    const handleOnchangeSubject = (e) => {
        setSubject(e.target.value)
    }
    const handleChangeCountry = (e) => {
        setCountry(e.target.value);
    };

    return (
        <Modal
            open={modalAddOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container>
                    <Grid item xs={12} my={3}>
                        <TextField id="inp-first-name" label="Firstname" variant="outlined" onChange={handleOnchangeFirstname} fullWidth />
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <TextField id="inp-last-name" label="Lastname" variant="outlined" onChange={handleOnchangeLastname} fullWidth />
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <TextField id="inp-subject" label="Subject" variant="outlined" onChange={handleOnchangeSubject} fullWidth />
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <FormControl fullWidth>
                            <InputLabel id="label-country">Country</InputLabel>
                            <Select
                                id="select-country"
                                value="vietnam"
                                label="Country"
                                onChange={handleChangeCountry}
                            >
                                <MenuItem value="vietnam">Vietnam</MenuItem>
                                <MenuItem value="usa">USA</MenuItem>
                                <MenuItem value="china">China</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} my={3}>
                        <Button sx={{ marginRight: 5 }} variant="contained" color="success" onClick={handleAddNewUser}>Them</Button>
                        <Button onClick={handleModalClose} variant="contained" color="error">Huy</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
};

export default UserModalAdd;