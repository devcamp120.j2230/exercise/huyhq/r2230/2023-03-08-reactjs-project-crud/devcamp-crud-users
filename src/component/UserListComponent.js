import { Container, CircularProgress, Grid, TableContainer, TableHead, TableRow, TableCell, TableBody, Button, Paper, Table, Pagination } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchUsers } from "../action/user.action";
import { USERS_DELETE_ID, USERS_EDIT_ID, MODAL_UPDATE_SET, MODAL_DELETE_SET } from "../constants/user.constant";
import UserModalDelete from "./UserModalDelete";
import UserModalEdit from "./UserModalEdit";

const UserListComponent = () => { 
    const [userId, setUserId] = useState("");

    const dispatch = useDispatch();

    const { users, pending, changeStatus, } = useSelector((reduxData) => {
        return reduxData.userListReducer;
    });

    //Load Api User List to front-end
    useEffect(() => {
        dispatch(fetchUsers());
    }, [changeStatus]);

    //click button edit user with Id
    const onClickButtonEdit = (e) => {
        setUserId(e.target.dataset.id);
        dispatch({
            type: MODAL_UPDATE_SET,
            modalUpdateOpen: true,
        });
    }

    //click button delete user with Id
    const onClickButtonDelete = (e) => {
        setUserId(e.target.dataset.id);
        dispatch({
            type: MODAL_DELETE_SET,
            modalDeleteOpen: true
        })
    };

    return (
        <Container>
            {
                pending
                    ?
                    <Grid container justifyContent='center'>
                        <CircularProgress />
                    </Grid>
                    :
                    <Grid container justifyContent='center'>
                        <Grid item xs={12}>
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Id</TableCell>
                                            <TableCell align="right">Firstname</TableCell>
                                            <TableCell align="right">Lastname</TableCell>
                                            <TableCell align="right">Country</TableCell>
                                            <TableCell align="right">Subject</TableCell>
                                            <TableCell align="right">Custom Type</TableCell>
                                            <TableCell align="right">Register Status</TableCell>
                                            <TableCell align="center">Action</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {users.map((row) => {
                                            return <TableRow
                                                key={row.id}
                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                            >
                                                <TableCell component="th" scope="row">
                                                    {row.id}
                                                </TableCell>
                                                <TableCell align="right">{row.firstname}</TableCell>
                                                <TableCell align="right">{row.lastname}</TableCell>
                                                <TableCell align="right">{row.country}</TableCell>
                                                <TableCell align="right">{row.subject}</TableCell>
                                                <TableCell align="right">{row.customerType}</TableCell>
                                                <TableCell align="right">{row.registerStatus}</TableCell>
                                                <TableCell align="center">
                                                    <Button variant="contained" color="info" sx={{ marginRight: "3px" }} onClick={onClickButtonEdit} data-id={row.id}>Sửa</Button>
                                                    <Button variant="contained" color="error" onClick={onClickButtonDelete} data-id={row.id}>Xóa</Button>
                                                </TableCell>
                                            </TableRow>
                                        })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </Grid>
            }
            <UserModalEdit userId={userId}/>
            <UserModalDelete userId={userId}/>
        </Container>
    )
}

export default UserListComponent;